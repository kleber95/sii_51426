// Este codigo ha sido modificado por Kleber Quinatoa (51426)
// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
